# Autotagger

This module allows you to automatically assign taxonomy terms to nodes
according to the text you have entered.

We can select for each type of content which taxonomy we want to use and in
which text fields we want to search for matches.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/autotagger).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/autotagger).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- It's possible to add options to any content type.


## Maintainers

- Alvaro Hurtado - [alvar0hurtad0](https://alvar0hurtad0.es)
