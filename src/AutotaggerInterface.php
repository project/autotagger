<?php

namespace Drupal\autotagger;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for autotagger plugins.
 */
interface AutotaggerInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the translated plugin description.
   *
   * @return string
   *   The translated description.
   */
  public function description();

  /**
   * Returns whether the plugin is configurable by bundle or not.
   *
   * @return bool
   *   Is this plugin configurable by bundle?
   */
  public function isConfigurable();

  /**
   * Adds options to the form.
   *
   * @param object $form
   *   The form object.
   * @param \Drupal\autotagger\FormStateInterface $form_state
   *   The form state object.
   * @param string $form_id
   *   The form ID.
   */
  public function addFormOptions(&$form, FormStateInterface $form_state, $form_id);

}
