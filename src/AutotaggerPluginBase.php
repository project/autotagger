<?php

namespace Drupal\autotagger;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for autotagger plugins.
 */
abstract class AutotaggerPluginBase extends PluginBase implements AutotaggerInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigurable() {
    return $this->pluginDefinition['configurable'] === 'true';
  }

}
