<?php

namespace Drupal\autotagger\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines autotagger annotation object.
 *
 * @Annotation
 */
class Autotagger extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Is this plugin configurable by bundle.
   *
   * @var bool
   */
  public $configurable;

}
