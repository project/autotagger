<?php

namespace Drupal\autotagger;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Autotagger plugin manager.
 */
class AutotaggerPluginManager extends DefaultPluginManager {

  /**
   * Constructs AutotaggerPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Autotagger',
      $namespaces,
      $module_handler,
      'Drupal\autotagger\AutotaggerInterface',
      'Drupal\autotagger\Annotation\Autotagger'
    );
    $this->alterInfo('autotagger_info');
    $this->setCacheBackend($cache_backend, 'autotagger_plugins');
  }

}
