<?php

namespace Drupal\Tests\autotagger\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\VocabularyInterface;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Test the Search in text fields plugin.
 *
 * @group autotagger
 */
class SearchInTextFieldsTest extends KernelTestBase {
  /**
   * An array of module names.
   *
   * @var array
   * */
  protected static $modules = [
    'autotagger',
    'autotagger_search_in_text',
    'system',
    'user',
    'node',
    'field',
    'text',
    'taxonomy',
    'filter',
  ];

  use NodeCreationTrait;
  use UserCreationTrait;
  use ContentTypeCreationTrait;
  use TaxonomyTestTrait;

  /**
   * The node type.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected NodeType $contentType;

  /**
   * The taxonomy vocabulary.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected VocabularyInterface $vocabulary;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences']);

    // Install *module* schema for node/user modules.
    $this->installSchema('node', ['node_access']);
    $this->installSchema('user', ['users_data']);

    // Install *entity* schema for the node entity.
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installEntitySchema('taxonomy_term');

    $this->installConfig(['field', 'node', 'text', 'user', 'taxonomy']);

    $this->owner = $this->createUser([], 'testuser');

    // Create a vocabulary with some terms.
    $this->vocabulary = $this->createVocabulary();
    $this->createTerm($this->vocabulary, ['name' => 'Bahamontes']);
    $this->createTerm($this->vocabulary, ['name' => 'Perico']);
    $this->createTerm($this->vocabulary, ['name' => 'Indurain']);
    $this->createTerm($this->vocabulary, ['name' => 'Valverde']);
    $this->createTerm($this->vocabulary, ['name' => 'Zugasti']);

    $this->createContentType([
      'name' => 'Test node type',
      'type' => 'test_node_type',
    ]);

    // Add the term field.
    FieldStorageConfig::create([
      'field_name' => 'field_term',
      'type' => 'entity_reference',
      'entity_type' => 'node',
      'cardinality' => 1,
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ])->save();
    FieldConfig::create([
      'field_name' => 'field_term',
      'entity_type' => 'node',
      'bundle' => 'test_node_type',
      'label' => 'Terms',
      'settings' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            $this->vocabulary->id() => $this->vocabulary->id(),
          ],
          'auto_create' => FALSE,
        ],
      ],
    ])->save();

    // Add a textfield.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_string_no_default',
      'entity_type' => 'node',
      'type' => 'string',
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'test_node_type',
    ]);

    $field->save();
  }

  /**
   * Test the configuration options got the search_in_text_fields plugin.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testConfiguration() {
    $nodeType = NodeType::load('test_node_type');

    // Add a third party setting.
    $nodeType->setThirdPartySetting('autotagger', 'search_in_text_fields', $this->configurationValues());
    $nodeType->save();

    $this->assertSame($this->configurationValues(), $nodeType->getThirdPartySetting('autotagger', 'search_in_text_fields'));
  }

  /**
   * Data provider for testNodesTagging().
   *
   * @return array
   *   A list of testNodesTagging method arguments.
   */
  public function nodesTaggingProvider() {
    return [
      [
        'Bahamontes is the biggest idol of the beginning of cycling',
        ['Bahamontes'],
        ['Perico'],
      ],
      [
        'Valverde is from Murcia, but Zugasti is from Lezo',
        ['Valverde', 'Zugasti'],
        ['Bahamontes', 'Perico'],
      ],
    ];
  }

  /**
   * Test the tagging process.
   *
   * @param string $title
   *   The node title.
   * @param array $expected
   *   The expected terms.
   * @param array $notExpected
   *   The not expected terms.
   *
   * @dataProvider nodesTaggingProvider
   */
  public function testNodesTagging(string $title, array $expected, array $notExpected) {
    $nodeType = NodeType::load('test_node_type');
    $nodeType->setThirdPartySetting('autotagger', 'search_in_text_fields', $this->configurationValues());
    $nodeType->save();

    $node = Node::create([
      'title' => $title,
      'type' => 'test_node_type',
    ]);
    $node->save();

    $termsInNode = $this->getTermsFromNode($node);

    $this->assertEqualsCanonicalizing($expected, $termsInNode);

    foreach ($notExpected as $term) {
      $this->assertNotContains($term, $termsInNode);
    }
  }

  /**
   * Helper function to get all terms labels for a given node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return array
   *   An array of terms.
   */
  protected function getTermsFromNode(NodeInterface $node) {
    $terms = [];
    foreach ($node->field_term->getValue() as $term_reference) {
      $term_id = array_pop($term_reference);
      if ($term_id) {
        $terms[] = Term::load($term_id)->label();
      }
    }
    return $terms;
  }

  /**
   * Helper method to get the configuration values in simplest scenario.
   *
   * @return array
   *   An array of configuration values.
   */
  protected function configurationValues() {
    return [
      'autotagger_source_field' => ['title'],
      'autotagger_destination_field' => 'field_term',
      'autotagger_tag_on_create_only' => '',
      'autotagger_rebuild_on_submit' => '',
    ];
  }

}
