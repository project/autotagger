<?php

namespace Drupal\autotagger_search_in_text\Plugin\Autotagger;

use Drupal\autotagger\AutotaggerPluginBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\node\NodeTypeInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the autotagger.
 *
 * @Autotagger(
 *   id = "search_in_text_fields",
 *   label = @Translation("Search in Text fields"),
 *   description = @Translation("Searches in text fields and adds tags based on text matches."),
 *   configurable = false
 * )
 */
final class SearchInTextFields extends AutotaggerPluginBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new self($configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get("entity_type.manager");
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function addFormOptions(&$form, FormStateInterface $form_state, $form_id) {
    if ($form_id != 'node_type_add_form' && $form_id != 'node_type_edit_form') {
      return;
    }

    $source_fields = [];
    $destination_fields = [];

    $default_values = $form_state->getFormObject()->getEntity()->getThirdPartySetting('autotagger', 'search_in_text_fields');
    $form['autotagger'] = [
      '#type' => 'details',
      '#group' => 'additional_settings',
      '#title' => 'Autotagger : ' . $this->label(),
      '#description' => $this->description(),
    ];

    $type = $form['type']['#default_value'];

    $fields = $this->entityFieldManager->getFieldDefinitions('node', $type);

    /** @var \Drupal\field\Entity\FieldConfig $field */
    foreach ($fields as $field) {
      $field_type = $field->getType();

      if (in_array($field_type, $this->getSourceFieldsCandidates())) {
        $source_fields[$field->getName()] = $field->getLabel();
      }

      // Text fields on node or media references can be used as a source.
      $target_types = ['node', 'media'];
      if ($field_type == 'entity_reference' && (in_array($field->getSettings()['target_type'], $target_types))) {
        $source_fields[$field->getName()] = $this->t('Text fields on entities referenced by @label', ['@label' => $field->getLabel()])->render();
      }

      // Text fields on paragraphs can be used as a source.
      if ($field_type == 'entity_reference_revisions') {
        $source_fields[$field->getName()] = $this->t('Text fields on paragraphs referenced by @label', ['@label' => $field->getLabel()])->render();
      }

      // Taxonomy references can be used as a destination.
      if ($field_type == 'entity_reference' && $field->getSettings()['target_type'] == 'taxonomy_term') {
        $destination_fields[$field->getName()] = $field->getLabel();
      }
    }

    $form['autotagger']['autotagger_source_field'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Source fields'),
      '#description' => $this->t('Choose which fields to scan for terms. Checking an entity reference field will cause all text fields on the referenced entity to be scanned.'),
      '#default_value' => $default_values['autotagger_source_field'] ?? [],
      '#options' => $source_fields,
      '#access' => TRUE,
    ];

    $form['autotagger']['autotagger_destination_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination fields'),
      '#description' => $this->t('Choose the taxonomy reference field to use. The vocabularies enabled on this field will provide the scanned for terms.'),
      '#default_value' => $default_values['autotagger_destination_field'] ?? '',
      '#options' => $destination_fields,
      '#access' => TRUE,
    ];

    $form['autotagger']['autotagger_tag_on_create_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only assign tags on initial node creation.'),
      '#default_value' => $default_values['autotagger_tag_on_create_only'] ?? 0,
      '#access' => TRUE,
    ];

    $form['#entity_builders'][] = [$this, 'entityBuilder'];
  }

  /**
   * {@inheritdoc}
   */
  public function entityPresave(NodeInterface $node) {
    // Determine if this node is set to tag only on node creation.
    $storage = $this->entityTypeManager->getStorage($node->getEntityType()->getBundleEntityType());
    $entity_type = $storage->load($node->bundle());

    if (!$entity_type instanceof ConfigEntityInterface) {
      return;
    }

    $settings = $entity_type->getThirdPartySetting('autotagger', 'search_in_text_fields');

    // If settings are empty, them skip the tagging.
    if (!$settings) {
      return;
    }

    $create_only = $settings['autotagger_tag_on_create_only'] ?? FALSE;

    if ($node->isNew() || !$create_only) {
      // If the node is first being created, or the node is set to not only tag
      // on new, then go ahead and tag it.
      $this->tagNode($node);
    }
  }

  /**
   * Entity builder for the menu configuration entity.
   */
  public function entityBuilder($entity_type, NodeTypeInterface $node_type, array &$form, FormStateInterface $form_state) {
    $autotagger = [
      'autotagger_source_field' => $form_state->getValue('autotagger_source_field'),
      'autotagger_destination_field' => $form_state->getValue('autotagger_destination_field'),
      'autotagger_tag_on_create_only' => $form_state->getValue('autotagger_tag_on_create_only'),
    ];
    $node_type->setThirdPartySetting('autotagger', 'search_in_text_fields', $autotagger);
  }

  /**
   * Get all the source field types that might be used in configuration.
   *
   * @return string[]
   *   The list of source field types.
   */
  protected function getSourceFieldsCandidates() {
    return [
      'text',
      'string',
      'text_with_summary',
      'string_with_summary',
      'text_long',
      'string_long',
    ];
  }

  /**
   * Tags a node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to tag.
   */
  protected function tagNode(NodeInterface &$node): void {
    $node_type = NodeType::load($node->bundle());
    $autotag_values = $node_type->getThirdPartySetting('autotagger', 'search_in_text_fields');

    $source = $autotag_values['autotagger_source_field'];
    $destination = $autotag_values['autotagger_destination_field'];

    if (is_null($source) || is_null($destination)) {
      // If either the source or destination is not set, we cannot proceed.
      return;
    }

    $text_array = [];
    $text = '';
    foreach ($source as $field) {
      if (!empty($field) && $node->hasField($field)) {
        $definition = $node->getFieldDefinition($field);

        if ($definition->getType() == 'entity_reference_revisions') {
          // For paragraph fields, process all the text fields on the
          // referenced paragraph and any nested paragraphs.
          if (!empty($node->{$field})) {
            foreach ($node->{$field} as $item) {
              if ($item->entity) {
                $referenced_entity = $item->entity;
                $this->processParagraphEntity($referenced_entity, $text_array);
              }
            }
          }
        }
        elseif ($definition->getType() == 'entity_reference') {
          // For entity reference fields, process all the text fields on the
          // referenced entity.
          if (!empty($node->{$field})) {
            foreach ($node->{$field} as $item) {
              if ($item->entity) {
                $referenced_entity = $item->entity;
                $this->processReferencedEntity($referenced_entity, $text_array);
              }
            }
          }
        }
        elseif (in_array($definition->getType(), $this->getSourceFieldsCandidates())) {
          // For text fields, get the value(s).
          if (!empty($node->{$field})) {
            $this->processTextField($node->{$field}, $text_array);
          }
        }
      }
    }

    // Combine all the text from all the fields into one string to search.
    foreach ($text_array as $item) {
      if (isset($item['summary'])) {
        $text .= ' ' . trim(strtolower($item['summary'])) . ' ';
      }
      if (isset($item['value'])) {
        $text .= ' ' . trim(strtolower($item['value'])) . ' ';
      }
    }

    // Get all possible values.
    $values = $node->getFieldDefinition($destination)
      ->getFieldStorageDefinition()
      ->getOptionsProvider($destination, $node)
      ->getPossibleValues();
    $tree = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadMultiple($values);

    // Tag if applies.
    foreach ($tree as $term) {
      if ($this->search($term->label(), $text)) {
        $this->tag($node, $destination, $term);
      }
    }
  }

  /**
   * Adds a term to a field.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Full node object that should be tagged.
   * @param string $field_name
   *   Machine name of the field to hold the term.
   * @param \Drupal\taxonomy\TermInterface $term
   *   Term to be added to $field on $node.
   */
  protected function tag(NodeInterface $node, string $field_name, TermInterface $term) {
    $node_tags = $node->get($field_name)->getValue();
    $node_tags_ids = array_column($node_tags, 'target_id');
    if (!is_null($term) && !in_array($term->id(), $node_tags_ids)) {
      $tid = $term->id();
      $value = ['target_id' => $tid];
      $node->{$field_name}->appendItem($value);
    }
  }

  /**
   * Get the values from all text fields on the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity from an entity reference field.
   * @param array $text_array
   *   Array that collects the values from the fields.
   */
  protected function processReferencedEntity(ContentEntityInterface $entity, array &$text_array) {
    $fields = $entity->getFields();
    foreach ($fields as $name => $field) {
      $type = $entity->getFieldDefinition($name)->getType();
      if (in_array($type, $this->getSourceFieldsCandidates())) {
        $this->processTextField($field, $text_array);
      }
    }
  }

  /**
   * Get the values from all text fields on the paragraph and nested paragraphs.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Paragraph entity from an entity reference revisions field.
   * @param array $text_array
   *   Array that collects the values from the fields.
   */
  protected function processParagraphEntity(ContentEntityInterface $entity, array &$text_array) {
    $fields = $entity->getFields();
    foreach ($fields as $name => $field) {
      $type = $entity->getFieldDefinition($name)->getType();

      if (in_array($type, $this->getSourceFieldsCandidates())) {
        $this->processTextField($field, $text_array);
      }

      if ($type == 'entity_reference_revisions') {
        // If we hit another paragraph field, drill down into that.
        $referenced_entity = $entity->{$name}->entity;
        if (!is_null($referenced_entity)) {
          $this->processParagraphEntity($referenced_entity, $text_array);
        }
      }
    }
  }

  /**
   * Extract the text data from a text field.
   *
   * @param \Drupal\Core\Field\FieldItemList $field
   *   The field to parse for text values.
   * @param array $text_array
   *   The array storing all the text values.
   */
  protected function processTextField(FieldItemList $field, array &$text_array) {
    foreach ($field->getIterator() as $item) {
      $text_array[] = $item->getValue();
    }
  }

  /**
   * Search for a given string in given text.
   *
   * @param string $needle
   *   Text to search for.
   * @param string $haystack
   *   Text to search in.
   *
   * @return bool
   *   True if found.
   */
  protected function search($needle, $haystack) {
    // Normalize the strings to NFC to handle accented characters.
    $needle = \Normalizer::normalize($needle, \Normalizer::FORM_C);
    $haystack = \Normalizer::normalize($haystack, \Normalizer::FORM_C);

    $text = trim(preg_replace("/[^\p{L}0-9]+/u", " ", $needle));
    $needle = ' ' . strtolower($text) . ' ';

    $text = trim(preg_replace("/[^\p{L}0-9]+/u", " ", $haystack));
    $haystack = ' ' . strtolower($text) . ' ';

    return strpos($haystack, $needle) !== FALSE;
  }

}
